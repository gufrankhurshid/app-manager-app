package com.khurshid.gufran.appmanager.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import com.khurshid.gufran.appmanager.R;
import com.khurshid.gufran.appmanager.adapter.HomeActivityPagerAdapter;
import com.khurshid.gufran.appmanager.fragments.InstalledAppFragment;
import com.khurshid.gufran.appmanager.fragments.UninstalledAppFragment;
import com.khurshid.gufran.appmanager.fragments.UpdatedAppFragment;
import com.khurshid.gufran.appmanager.management.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;

/*
 *  Code Prepared by **Gufran Khurshid**.
 *  Sr Android Developer.
    Email Id : gufran.khurshid@gmail.com
    Skype Id : gufran.khurshid
    Code: **16 January, 2016.**

    Description: **HomeActivity is the main activity hosting fragments**

    All Rights Reserved.
*/
public class HomeActivity extends BaseActivity {

    @InjectView(R.id.viewpager)
    ViewPager viewpager;
    @InjectView(R.id.tabLayout)
    TabLayout tabLayout;

    HomeActivityPagerAdapter homeActivityPagerAdapter;
    List<Fragment> fragmentList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void referView() {

    }

    @Override
    public void applyTypeFace() {

    }

    @Override
    public void initialize() {

        fragmentList = new ArrayList<>();
        fragmentList.add(InstalledAppFragment.newInstance());
        fragmentList.add(UninstalledAppFragment.newInstance());
        fragmentList.add(UpdatedAppFragment.newInstance());
        homeActivityPagerAdapter = new HomeActivityPagerAdapter(getSupportFragmentManager(), getApplicationContext(), fragmentList);
        viewpager.setAdapter(homeActivityPagerAdapter);
        tabLayout.setupWithViewPager(viewpager);


    }

    @Override
    public void setUpActionBar() {
    }

}
