package com.khurshid.gufran.appmanager.dao;

import com.khurshid.gufran.appmanager.persistence.PackageException;
import com.khurshid.gufran.appmanager.persistence.dto.App;

import java.util.List;

/**
 * Created by guffy on 1/16/16.
 */
public interface AppDao {
    public boolean saveApp(App app);

    public boolean updateApp(App app);

    public List<App> getApps();

    public App getApp(String applicationId) throws PackageException;

    public List<App> getInstalledApps();

    public List<App> getUpdatingApps();

    public List<App> getRemovedApps();

    public boolean convertToInstalledApp(String applicationId);

    public boolean convertToUpdatingApp(String applicationId);

    public boolean convertToRemovedApp(String applicationId);

}
