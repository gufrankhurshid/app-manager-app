package com.khurshid.gufran.appmanager.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;
import com.khurshid.gufran.appmanager.R;
import com.khurshid.gufran.appmanager.adapter.UpdatedAppAdapter;
import com.khurshid.gufran.appmanager.adapter.dto.UpdatedApp;
import com.khurshid.gufran.appmanager.dao.AppDaoImpl;
import com.khurshid.gufran.appmanager.management.BaseFragment;
import com.khurshid.gufran.appmanager.persistence.dto.App;
import com.khurshid.gufran.appmanager.persistence.dto.UpdatingApp;
import com.khurshid.gufran.appmanager.util.DateConverterUtil;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;

/**
 * Created by guffy on 1/15/16.
 */
public class UpdatedAppFragment extends BaseFragment {

    //-------------->> View Reference
    View mainView;
    @InjectView(R.id.contentLoadingProgressBar)
    ContentLoadingProgressBar contentLoadingProgressBar;
    @InjectView(R.id.updatedAppRecyclerView)
    RecyclerView updatedAppRecyclerView;


    List<UpdatedApp> updatedAppList;
    UpdatedAppAdapter updatedAppAdapter;


    @Inject
    AppDaoImpl appDao;

    public static UpdatedAppFragment newInstance() {
        UpdatedAppFragment fragment = new UpdatedAppFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView
                = inflater.inflate(R.layout.fragment_updated_app, container, false);
        return mainView;
    }

    @Override
    public void referView() {

    }

    @Override
    public void applyTypeFace() {

    }

    @Override
    public void initializeMembers() {
        updatedAppList = new ArrayList<UpdatedApp>();
        updatedAppAdapter = new UpdatedAppAdapter(updatedAppList, getActivity());
    }

    @Override
    public void initializeViews() {
        updatedAppRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        updatedAppRecyclerView.setLayoutManager(linearLayoutManager);

        updatedAppRecyclerView.setAdapter(updatedAppAdapter);

        updatedAppList.clear();
        new AppEnlister().execute(appDao.getUpdatingApps());
    }

    class AppEnlister extends AsyncTask<List<App>, Void, Void> {
        @Override
        protected void onPreExecute() {
            updatedAppRecyclerView.setVisibility(View.GONE);
            contentLoadingProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(List<App>... params) {
            for (App app :
                    params[0]) {
                UpdatingApp updatingApp = (UpdatingApp) app;
                updatedAppList.add(new UpdatedApp(updatingApp.getName(), updatingApp.getIconURI(), DateConverterUtil.convert(updatingApp.getLastUpdationDate(), ""), updatingApp.getNumberOfUpdation()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            updatedAppRecyclerView.setVisibility(View.VISIBLE);
            contentLoadingProgressBar.setVisibility(View.GONE);
            updatedAppAdapter.notifyDataSetChanged();
        }
    }
}
