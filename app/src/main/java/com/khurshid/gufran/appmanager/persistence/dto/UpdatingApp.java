package com.khurshid.gufran.appmanager.persistence.dto;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by guffy on 1/15/16.
 */
@DatabaseTable(tableName = "UpdatingApp")
public class UpdatingApp extends App {
    @DatabaseField
    int numberOfUpdation;
    @DatabaseField(dataType = DataType.DATE_STRING)
    Date lastUpdationDate;

    public UpdatingApp() {
    }

    public UpdatingApp(String name, String applicationId, String iconURI, int numberOfUpdation, Date lastUpdationDate) {
        super();
        this.name = name;
        this.applicationId = applicationId;
        this.iconURI = iconURI;
        this.status = Status.UPDATING;
        this.lastUpdationDate = lastUpdationDate;
        this.numberOfUpdation = numberOfUpdation;
    }


    public int getNumberOfUpdation() {
        return numberOfUpdation;
    }

    public void setNumberOfUpdation(int numberOfUpdation) {
        this.numberOfUpdation = numberOfUpdation;
    }

    public Date getLastUpdationDate() {
        return lastUpdationDate;
    }

    public void setLastUpdationDate(Date lastUpdationDate) {
        this.lastUpdationDate = lastUpdationDate;
    }
}
