package com.khurshid.gufran.appmanager.adapter.dto;

/**
 * Created by gufran on 17/1/16.
 */
public class InstalledApp {
    public String appName;
    public String fileUri;

    public InstalledApp(String appName, String fileUri) {
        this.appName = appName;
        this.fileUri = fileUri;
    }
}
