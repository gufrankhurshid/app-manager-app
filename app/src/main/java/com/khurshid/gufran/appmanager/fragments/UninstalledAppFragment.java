package com.khurshid.gufran.appmanager.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;
import com.khurshid.gufran.appmanager.R;
import com.khurshid.gufran.appmanager.adapter.RemovedAppAdapter;
import com.khurshid.gufran.appmanager.adapter.dto.RemovedApp;
import com.khurshid.gufran.appmanager.dao.AppDaoImpl;
import com.khurshid.gufran.appmanager.management.BaseFragment;
import com.khurshid.gufran.appmanager.persistence.dto.App;
import com.khurshid.gufran.appmanager.util.DateConverterUtil;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;

/**
 * Created by guffy on 1/15/16.
 */
public class UninstalledAppFragment extends BaseFragment {

    //-------------->> View Reference
    View mainView;
    @InjectView(R.id.contentLoadingProgressBar)
    ContentLoadingProgressBar contentLoadingProgressBar;
    @InjectView(R.id.removedAppRecyclerView)
    RecyclerView removedAppRecyclerView;

    List<RemovedApp> removedAppList;
    RemovedAppAdapter removedAppAdapter;

    @Inject
    AppDaoImpl appDao;

    public static UninstalledAppFragment newInstance() {
        UninstalledAppFragment fragment = new UninstalledAppFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView
                = inflater.inflate(R.layout.fragment_uninstalled_app, container, false);
        return mainView;
    }

    @Override
    public void referView() {

    }

    @Override
    public void applyTypeFace() {

    }

    @Override
    public void initializeMembers() {
        removedAppList = new ArrayList<RemovedApp>();
        removedAppAdapter = new RemovedAppAdapter(removedAppList, getActivity());
    }

    @Override
    public void initializeViews() {
        removedAppRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        removedAppRecyclerView.setLayoutManager(linearLayoutManager);

        removedAppRecyclerView.setAdapter(removedAppAdapter);

        removedAppList.clear();
        new AppEnlister().execute(appDao.getRemovedApps());
    }

    class AppEnlister extends AsyncTask<List<App>, Void, Void> {
        @Override
        protected void onPreExecute() {
            removedAppRecyclerView.setVisibility(View.GONE);
            contentLoadingProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(List<App>... params) {
            for (App app :
                    params[0]) {
                com.khurshid.gufran.appmanager.persistence.dto.RemovedApp removedApp = (com.khurshid.gufran.appmanager.persistence.dto.RemovedApp) app;
                removedAppList.add(new RemovedApp(removedApp.getName(), removedApp.getIconURI(), DateConverterUtil.convert(removedApp.getRemovalDate(), "")));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            removedAppRecyclerView.setVisibility(View.VISIBLE);
            contentLoadingProgressBar.setVisibility(View.GONE);
            removedAppAdapter.notifyDataSetChanged();
        }
    }
}
