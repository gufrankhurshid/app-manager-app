package com.khurshid.gufran.appmanager.receiver;

/**
 * Created by guffy on 1/17/16.
 */
public enum UpdationState {
    NOT_DEFIND, ADDED, REPLACED, REMOVED;
}
