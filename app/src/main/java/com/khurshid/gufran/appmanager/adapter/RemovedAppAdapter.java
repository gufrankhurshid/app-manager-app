package com.khurshid.gufran.appmanager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khurshid.gufran.appmanager.R;
import com.khurshid.gufran.appmanager.adapter.dto.RemovedApp;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by gufran on 17/1/16.
 */
public class RemovedAppAdapter extends RecyclerView.Adapter<RemovedAppAdapter.RemovedAppHolder> {

    List<RemovedApp> removedAppList;
    Context context;

    public RemovedAppAdapter(List<RemovedApp> removedAppList, Context context) {
        this.removedAppList = removedAppList;
        this.context = context;
    }

    @Override
    public RemovedAppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_removed_app, parent, false);
        RemovedAppHolder holder = new RemovedAppHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RemovedAppHolder holder, int position) {
        final RemovedApp removedApp = removedAppList.get(position);
        holder.appNameTextView.setText(removedApp.appName);
        holder.removalDateTextView.setText(removedApp.removalDateTimeString);

        File iconFile = new File(removedApp.fileUri);
        if (iconFile != null) {
            Picasso p = Picasso.with(context);
            p.load(iconFile).placeholder(R.mipmap.ic_launcher).into(holder.appIconImageView);
        }
    }

    @Override
    public int getItemCount() {
        return removedAppList.size();
    }

    class RemovedAppHolder extends RecyclerView.ViewHolder {
        ImageView appIconImageView;
        TextView appNameTextView;
        TextView removalDateTextView;

        public RemovedAppHolder(View itemView) {
            super(itemView);
            this.appIconImageView = (ImageView) itemView.findViewById(R.id.appIconImageView);
            this.appNameTextView = (TextView) itemView.findViewById(R.id.appNameTextView);
            this.removalDateTextView = (TextView) itemView.findViewById(R.id.removalDateTextView);
        }
    }
}
