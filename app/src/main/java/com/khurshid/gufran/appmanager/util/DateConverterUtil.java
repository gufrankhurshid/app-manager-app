package com.khurshid.gufran.appmanager.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by gufran on 17/1/16.
 */
public class DateConverterUtil {

    public static String convert(Date date, String format) {
        if (format.equals("")) format = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(date);
    }
}
