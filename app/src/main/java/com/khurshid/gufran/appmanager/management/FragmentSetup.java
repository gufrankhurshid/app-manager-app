package com.khurshid.gufran.appmanager.management;

/*
 *  Code Prepared by **Gufran Khurshid**.
 *  Sr Android Developer.
    Email Id : gufran.khurshid@gmail.com
    Skype Id : gufran.khurshid
    Code: **17 January, 2016.**

    Description: **FragmentSetup contains common methods**

    All Rights Reserved.
*/
public interface FragmentSetup {

    public void referView();

    public void applyTypeFace();

    public void initializeMembers();

    public void initializeViews();
}
