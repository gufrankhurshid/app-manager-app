package com.khurshid.gufran.appmanager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khurshid.gufran.appmanager.R;
import com.khurshid.gufran.appmanager.adapter.dto.UpdatedApp;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by gufran on 17/1/16.
 */
public class UpdatedAppAdapter extends RecyclerView.Adapter<UpdatedAppAdapter.UpdatedAppHolder> {

    List<UpdatedApp> updatedAppList;
    Context context;

    public UpdatedAppAdapter(List<UpdatedApp> updatedAppList, Context context) {
        this.updatedAppList = updatedAppList;
        this.context = context;
    }

    @Override
    public UpdatedAppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_updated_app, parent, false);
        UpdatedAppHolder holder = new UpdatedAppHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(UpdatedAppHolder holder, int position) {
        final UpdatedApp updatedApp = updatedAppList.get(position);
        holder.appNameTextView.setText(updatedApp.appName);
        holder.updationMessageTextView.setText("Updated " + updatedApp.numberOfTimesUpdated + " times");
        holder.lastUpdatedDateTextView.setText(updatedApp.lastUpdatedDateTimeString);
        File iconFile = new File(updatedApp.fileUri);
        if (iconFile != null) {
            Picasso p = Picasso.with(context);
            p.load(iconFile).placeholder(R.mipmap.ic_launcher).into(holder.appIconImageView);
        }
    }

    @Override
    public int getItemCount() {
        return updatedAppList.size();
    }

    class UpdatedAppHolder extends RecyclerView.ViewHolder {
        ImageView appIconImageView;
        TextView appNameTextView;
        TextView updationMessageTextView;
        TextView lastUpdatedDateTextView;

        public UpdatedAppHolder(View itemView) {
            super(itemView);
            this.appIconImageView = (ImageView) itemView.findViewById(R.id.appIconImageView);
            this.appNameTextView = (TextView) itemView.findViewById(R.id.appNameTextView);
            this.updationMessageTextView = (TextView) itemView.findViewById(R.id.updationMessageTextView);
            this.lastUpdatedDateTextView = (TextView) itemView.findViewById(R.id.lastUpdatedDateTextView);
        }
    }
}
