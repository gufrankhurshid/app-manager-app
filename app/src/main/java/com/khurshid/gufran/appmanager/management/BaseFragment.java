package com.khurshid.gufran.appmanager.management;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import roboguice.fragment.RoboFragment;

/*
 *  Code Prepared by **Gufran Khurshid**.
 *  Sr Android Developer.
    Email Id : gufran.khurshid@gmail.com
    Skype Id : gufran.khurshid
    Code: **17 January, 2016.**

    Description: **BaseFragment is used to intialize common variables and call common methods**

    All Rights Reserved.
*/
public abstract class BaseFragment extends RoboFragment implements FragmentSetup {

    public String TAG = AppManagerApp.TAG;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeMembers();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        referView();
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        applyTypeFace();
        initializeViews();
    }

}
