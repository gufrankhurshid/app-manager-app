package com.khurshid.gufran.appmanager.persistence;

/**
 * Created by guffy on 1/16/16.
 */
public class DuplicatePackageException extends PackageException {
    public DuplicatePackageException(String message) {
        super(message);
    }
}
