package com.khurshid.gufran.appmanager.adapter.dto;

/**
 * Created by gufran on 17/1/16.
 */
public class UpdatedApp {
    public String appName;
    public String fileUri;
    public int numberOfTimesUpdated;
    public String lastUpdatedDateTimeString;

    public UpdatedApp(String appName, String fileUri, String lastUpdatedDateTimeString, int numberOfTimesUpdated) {
        this.appName = appName;
        this.fileUri = fileUri;
        this.lastUpdatedDateTimeString = lastUpdatedDateTimeString;
        this.numberOfTimesUpdated = numberOfTimesUpdated;
    }
}
