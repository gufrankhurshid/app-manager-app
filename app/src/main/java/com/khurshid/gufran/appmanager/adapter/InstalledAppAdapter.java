package com.khurshid.gufran.appmanager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.khurshid.gufran.appmanager.R;
import com.khurshid.gufran.appmanager.adapter.dto.InstalledApp;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

/**
 * Created by gufran on 17/1/16.
 */
public class InstalledAppAdapter extends RecyclerView.Adapter<InstalledAppAdapter.InstalledAppHolder> {


    List<InstalledApp> installedAppList;
    Context context;

    public InstalledAppAdapter(List<InstalledApp> installedAppList, Context context) {
        this.installedAppList = installedAppList;
        this.context = context;
    }

    @Override
    public InstalledAppHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_installed_app, parent, false);
        InstalledAppHolder holder = new InstalledAppHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(InstalledAppHolder holder, int position) {
        final InstalledApp installedApp = installedAppList.get(position);
        holder.appNameTextView.setText(installedApp.appName);
        File iconFile = new File(installedApp.fileUri);
        if (iconFile != null) {
            Picasso p = Picasso.with(context);
            p.load(iconFile).placeholder(R.mipmap.ic_launcher).into(holder.appIconImageView);
        }

    }

    @Override
    public int getItemCount() {
        return installedAppList.size();
    }

    class InstalledAppHolder extends RecyclerView.ViewHolder {
        ImageView appIconImageView;
        TextView appNameTextView;

        public InstalledAppHolder(View itemView) {
            super(itemView);
            this.appIconImageView = (ImageView) itemView.findViewById(R.id.appIconImageView);
            this.appNameTextView = (TextView) itemView.findViewById(R.id.appNameTextView);
        }
    }
}
