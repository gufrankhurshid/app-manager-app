package com.khurshid.gufran.appmanager.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;
import com.khurshid.gufran.appmanager.R;
import com.khurshid.gufran.appmanager.adapter.InstalledAppAdapter;
import com.khurshid.gufran.appmanager.adapter.dto.InstalledApp;
import com.khurshid.gufran.appmanager.dao.AppDaoImpl;
import com.khurshid.gufran.appmanager.management.AppManagerApp;
import com.khurshid.gufran.appmanager.management.BaseFragment;
import com.khurshid.gufran.appmanager.persistence.dto.App;
import com.khurshid.gufran.appmanager.util.AppRetriever;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;

/**
 * Created by guffy on 1/15/16.
 */
public class InstalledAppFragment extends BaseFragment implements AppRetriever.OnPreparedListener {

    //-------------->> View Reference
    View mainView;
    @InjectView(R.id.contentLoadingProgressBar)
    ContentLoadingProgressBar contentLoadingProgressBar;
    @InjectView(R.id.installedAppRecyclerView)
    RecyclerView installedAppRecyclerView;


    List<InstalledApp> installedAppList;
    InstalledAppAdapter installedAppAdapter;

    AppRetriever appRetriever;

    @Inject
    AppDaoImpl appDao;

    public static InstalledAppFragment newInstance() {
        InstalledAppFragment fragment = new InstalledAppFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView
                = inflater.inflate(R.layout.fragment_installed_app, container, false);
        return mainView;
    }

    @Override
    public void referView() {

    }

    @Override
    public void applyTypeFace() {

    }

    @Override
    public void initializeMembers() {
        installedAppList = new ArrayList<InstalledApp>();
        installedAppAdapter = new InstalledAppAdapter(installedAppList, getActivity());


        List<App> appList = appDao.getApps();
        if (appList.size() <= 0) {
            Log.e(AppManagerApp.TAG, "No Application in AppManagerDB !");
            appRetriever = new AppRetriever(AppManagerApp.getAppContext());
            appRetriever.bindInstalledAppFragment(this);
            appRetriever.execute();
        }
    }

    @Override
    public void initializeViews() {
        installedAppRecyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        installedAppRecyclerView.setLayoutManager(gridLayoutManager);

        installedAppRecyclerView.setAdapter(installedAppAdapter);

        installedAppList.clear();
        new AppEnlister().execute(appDao.getInstalledApps());
    }


    class AppEnlister extends AsyncTask<List<App>, Void, Void> {
        @Override
        protected void onPreExecute() {
            installedAppRecyclerView.setVisibility(View.GONE);
            contentLoadingProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(List<App>... params) {
            for (App app :
                    params[0]) {
                installedAppList.add(new InstalledApp(app.getName(), app.getIconURI()));
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            installedAppRecyclerView.setVisibility(View.VISIBLE);
            contentLoadingProgressBar.setVisibility(View.GONE);
            installedAppAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void prepared() {
        new AppEnlister().execute(appDao.getInstalledApps());
    }
}
