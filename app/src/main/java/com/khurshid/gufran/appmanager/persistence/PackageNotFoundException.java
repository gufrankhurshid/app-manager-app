package com.khurshid.gufran.appmanager.persistence;

/**
 * Created by guffy on 1/16/16.
 */
public class PackageNotFoundException extends PackageException {
    public PackageNotFoundException(String message) {
        super(message);
    }
}
