package com.khurshid.gufran.appmanager.management;

/**
 * Created by Gufran Khurshid on 1/15/16.
 */
public interface ActivitySetup {

    void referView();

    void applyTypeFace();

    void initialize();

    void setUpActionBar();
}
