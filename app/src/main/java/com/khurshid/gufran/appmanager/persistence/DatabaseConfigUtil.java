package com.khurshid.gufran.appmanager.persistence;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.khurshid.gufran.appmanager.persistence.dto.App;
import com.khurshid.gufran.appmanager.persistence.dto.InstalledApp;
import com.khurshid.gufran.appmanager.persistence.dto.RemovedApp;
import com.khurshid.gufran.appmanager.persistence.dto.UpdatingApp;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by guffy on 1/15/16.
 */
class DatabaseConfigUtil extends OrmLiteConfigUtil {
    public static void main(String[] arg) throws SQLException, IOException {
        Class<App>[] classes = new Class[]{InstalledApp.class, InstalledApp.class, RemovedApp.class, UpdatingApp.class};
        writeConfigFile("ormlite_config.txt");
    }
}
