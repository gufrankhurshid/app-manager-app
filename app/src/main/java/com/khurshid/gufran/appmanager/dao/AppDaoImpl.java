package com.khurshid.gufran.appmanager.dao;

import android.content.Context;
import android.util.Log;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.khurshid.gufran.appmanager.management.AppManagerApp;
import com.khurshid.gufran.appmanager.persistence.DuplicatePackageException;
import com.khurshid.gufran.appmanager.persistence.PackageException;
import com.khurshid.gufran.appmanager.persistence.PackageNotFoundException;
import com.khurshid.gufran.appmanager.persistence.PersistenceManager;
import com.khurshid.gufran.appmanager.persistence.dto.App;
import com.khurshid.gufran.appmanager.persistence.dto.InstalledApp;
import com.khurshid.gufran.appmanager.persistence.dto.RemovedApp;
import com.khurshid.gufran.appmanager.persistence.dto.Status;
import com.khurshid.gufran.appmanager.persistence.dto.UpdatingApp;

import java.util.Date;
import java.util.List;

/**
 * Created by guffy on 1/16/16.
 */
@Singleton
public class AppDaoImpl implements AppDao {


    @Inject
    PersistenceManager persistenceManager;


    public AppDaoImpl() {
    }

    public AppDaoImpl(Context context) {
        persistenceManager = new PersistenceManager(context);
    }


    @Override
    public boolean saveApp(App app) {
        return persistenceManager.save(app);
    }

    @Override
    public boolean updateApp(App app) {
        return persistenceManager.update(app);
    }

    @Override
    public App getApp(String applicationId) throws PackageException {
        List<App> appList = persistenceManager.getApp(applicationId);
        if (appList.size() > 0) {
            if (appList.size() > 1) {
                throw new DuplicatePackageException("DUPLICATE PACKAGE FOUND for ID " + applicationId);
            }
            return appList.get(0);
        } else {
            throw new PackageNotFoundException("No such package with ID " + applicationId);
        }
    }

    @Override
    public List<App> getApps() {
        return persistenceManager.getAllApps();
    }

    @Override
    public List<App> getInstalledApps() {
        return persistenceManager.getApps(Status.INSTALLED);
    }

    @Override
    public List<App> getUpdatingApps() {
        return persistenceManager.getApps(Status.UPDATING);
    }

    @Override
    public List<App> getRemovedApps() {
        return persistenceManager.getApps(Status.REMOVED);
    }

    @Override
    public boolean convertToInstalledApp(String applicationId) {
        try {
            App alreadyExistingApp = getApp(applicationId);
            if (alreadyExistingApp != null) {
                if (alreadyExistingApp instanceof RemovedApp) {
                    InstalledApp installedApp = new InstalledApp();
                    installedApp.setName(alreadyExistingApp.getName());
                    installedApp.setIconURI(alreadyExistingApp.getIconURI());
                    installedApp.setApplicationId(alreadyExistingApp.getApplicationId());
                    installedApp.setStatus(Status.INSTALLED);
                    boolean isSaved = persistenceManager.save(installedApp);
                    boolean isRemoved = persistenceManager.delete(alreadyExistingApp);
                    return isSaved && isRemoved;
                }
            }
        } catch (DuplicatePackageException e) {
            Log.e(AppManagerApp.TAG, "DuplicatePackageException " + e.getMessage());
        } catch (PackageNotFoundException e) {
            Log.e(AppManagerApp.TAG, "PackageNotFoundException " + e.getMessage());
        } catch (Exception e) {
            Log.e(AppManagerApp.TAG, "Exception " + e.getMessage());
        }
        return false;
    }

    @Override
    public boolean convertToUpdatingApp(String applicationId) {
        try {
            App alreadyExistingApp = getApp(applicationId);
            if (alreadyExistingApp != null) {
                if (alreadyExistingApp instanceof InstalledApp) {
                    UpdatingApp updatingApp = new UpdatingApp();
                    updatingApp.setName(alreadyExistingApp.getName());
                    updatingApp.setIconURI(alreadyExistingApp.getIconURI());
                    updatingApp.setApplicationId(alreadyExistingApp.getApplicationId());
                    updatingApp.setStatus(Status.UPDATING);
                    updatingApp.setNumberOfUpdation(1);
                    updatingApp.setLastUpdationDate(new Date());

                    boolean isSaved = persistenceManager.save(updatingApp);
                    boolean isRemoved = persistenceManager.delete(alreadyExistingApp);
                    return isSaved && isRemoved;

                } else if (alreadyExistingApp instanceof UpdatingApp) {
                    UpdatingApp updatingApp = (UpdatingApp) alreadyExistingApp;
                    updatingApp.setStatus(Status.UPDATING);
                    updatingApp.setNumberOfUpdation(updatingApp.getNumberOfUpdation() + 1);
                    updatingApp.setLastUpdationDate(new Date());
                    return persistenceManager.update(updatingApp);
                }
            }

        } catch (DuplicatePackageException e) {
            Log.e(AppManagerApp.TAG, "DuplicatePackageException " + e.getMessage());
        } catch (PackageNotFoundException e) {
            Log.e(AppManagerApp.TAG, "PackageNotFoundException " + e.getMessage());
        } catch (Exception e) {
            Log.e(AppManagerApp.TAG, "Exception " + e.getMessage());
        }
        return false;
    }

    @Override
    public boolean convertToRemovedApp(String applicationId) {
        try {
            App alreadyExistingApp = getApp(applicationId);
            if (alreadyExistingApp != null) {
                RemovedApp removedApp = new RemovedApp();
                removedApp.setName(alreadyExistingApp.getName());
                removedApp.setIconURI(alreadyExistingApp.getIconURI());
                removedApp.setApplicationId(alreadyExistingApp.getApplicationId());
                removedApp.setStatus(Status.REMOVED);
                removedApp.setRemovalDate(new Date());
                boolean isSaved = persistenceManager.save(removedApp);
                boolean isRemoved = persistenceManager.delete(alreadyExistingApp);
                return isSaved && isRemoved;
            }
        } catch (DuplicatePackageException e) {
            Log.e(AppManagerApp.TAG, "DuplicatePackageException " + e.getMessage());
            e.printStackTrace();
        } catch (PackageNotFoundException e) {
            Log.e(AppManagerApp.TAG, "PackageNotFoundException " + e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            Log.e(AppManagerApp.TAG, "Exception " + e.getMessage());
            e.printStackTrace();
        }
        return false;
    }

}
