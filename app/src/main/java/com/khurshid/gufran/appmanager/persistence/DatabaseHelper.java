package com.khurshid.gufran.appmanager.persistence;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.khurshid.gufran.appmanager.R;
import com.khurshid.gufran.appmanager.persistence.dto.InstalledApp;
import com.khurshid.gufran.appmanager.persistence.dto.RemovedApp;
import com.khurshid.gufran.appmanager.persistence.dto.UpdatingApp;

import java.sql.SQLException;

/**
 * Created by guffy on 1/15/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "AppManager.db";
    private static final int DATABASE_VERSION = 1;

    Dao<InstalledApp, Integer> installedAppDao = null;
    RuntimeExceptionDao<InstalledApp, Integer> installedAppExceptionDao = null;

    Dao<RemovedApp, Integer> removedAppDao = null;
    RuntimeExceptionDao<RemovedApp, Integer> removedAppExceptionDao = null;

    Dao<UpdatingApp, Integer> updatingAppDao = null;
    RuntimeExceptionDao<UpdatingApp, Integer> updatingAppExceptionDao = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION,
                R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
        try {
            TableUtils.createTable(connectionSource, InstalledApp.class);
            TableUtils.createTable(connectionSource, UpdatingApp.class);
            TableUtils.createTable(connectionSource, RemovedApp.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
                          int arg3) {
        try {
            TableUtils.dropTable(connectionSource, InstalledApp.class, true);
            TableUtils.dropTable(connectionSource, UpdatingApp.class, true);
            TableUtils.dropTable(connectionSource, RemovedApp.class, true);
            onCreate(arg0, arg1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    Dao<InstalledApp, Integer> getInstalledAppDao() throws SQLException {
        if (installedAppDao == null) {
            installedAppDao = getDao(InstalledApp.class);
        }
        return installedAppDao;
    }

    Dao<UpdatingApp, Integer> getUpdatingAppDao() throws SQLException {
        if (updatingAppDao == null) {
            updatingAppDao = getDao(UpdatingApp.class);
        }
        return updatingAppDao;
    }

    Dao<RemovedApp, Integer> getRemovedAppDao() throws SQLException {
        if (removedAppDao == null) {
            removedAppDao = getDao(RemovedApp.class);
        }
        return removedAppDao;
    }


    RuntimeExceptionDao<InstalledApp, Integer> getInstalledAppRuntimeExceptionDao() {
        if (installedAppExceptionDao == null) {
            installedAppExceptionDao = getRuntimeExceptionDao(InstalledApp.class);
        }
        return installedAppExceptionDao;
    }


    RuntimeExceptionDao<UpdatingApp, Integer> getUpdatingAppRuntimeExceptionDao() {
        if (updatingAppExceptionDao == null) {
            updatingAppExceptionDao = getRuntimeExceptionDao(UpdatingApp.class);
        }
        return updatingAppExceptionDao;
    }

    RuntimeExceptionDao<RemovedApp, Integer> getRemovedAppRuntimeExceptionDao() {
        if (removedAppExceptionDao == null) {
            removedAppExceptionDao = getRuntimeExceptionDao(RemovedApp.class);
        }
        return removedAppExceptionDao;
    }

}

