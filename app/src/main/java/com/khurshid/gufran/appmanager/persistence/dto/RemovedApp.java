package com.khurshid.gufran.appmanager.persistence.dto;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by guffy on 1/15/16.
 */
@DatabaseTable(tableName = "RemovedApp")
public class RemovedApp extends App {
    @DatabaseField(dataType = DataType.DATE_STRING)
    Date removalDate;

    public RemovedApp() {
    }

    public RemovedApp(String name, String applicationId, String iconURI, Date removalDate) {
        this.name = name;
        this.applicationId = applicationId;
        this.iconURI = iconURI;
        this.removalDate = removalDate;
        this.status = Status.REMOVED;
    }

    public Date getRemovalDate() {
        return removalDate;
    }

    public void setRemovalDate(Date removalDate) {
        this.removalDate = removalDate;
    }
}
