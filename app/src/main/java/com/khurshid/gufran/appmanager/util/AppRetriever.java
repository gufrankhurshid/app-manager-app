package com.khurshid.gufran.appmanager.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import com.google.inject.Inject;
import com.khurshid.gufran.appmanager.dao.AppDaoImpl;
import com.khurshid.gufran.appmanager.fragments.InstalledAppFragment;
import com.khurshid.gufran.appmanager.helper.BitmapHelper;
import com.khurshid.gufran.appmanager.management.AppManagerApp;
import com.khurshid.gufran.appmanager.persistence.dto.InstalledApp;

import java.util.List;

/**
 * Created by guffy on 1/16/16.
 */
public class AppRetriever extends AsyncTask<Void, Integer, Integer> {
    @Inject
    AppDaoImpl appDao;
    Context context;
    @Inject
    BitmapHelper bitmapHelper;

    OnPreparedListener onPreparedListener;


    public AppRetriever(Context context) {
        this.context = context;
        bitmapHelper = new BitmapHelper(context);
    }

    @Override
    protected void onPreExecute() {
        appDao = new AppDaoImpl(context);
        Log.i(AppManagerApp.TAG, "Starting AppRetriever... ");
    }

    @Override
    protected Integer doInBackground(Void... params) {
        int count = 0;
        final PackageManager pm = AppManagerApp.getAppContext().getPackageManager();
        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            String name;
            String packageName;
            /* Use package name if app label is empty */
            if ((name = String.valueOf(pm.getApplicationLabel(packageInfo))).isEmpty()) {
                name = packageInfo.packageName;
            }
            packageName = packageInfo.packageName;
            Drawable icon = pm.getApplicationIcon(packageInfo);
            String iconPath = "";
            if (icon instanceof BitmapDrawable) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) icon;
                iconPath = bitmapHelper.storeIcon(bitmapDrawable.getBitmap()) + "";
            }
            InstalledApp installedApp = new InstalledApp(name, packageName, iconPath);
            appDao.saveApp(installedApp);

            publishProgress(++count);

        }
        return count;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Log.i(AppManagerApp.TAG, "Retreived  app ... " + values[0]);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        Log.i(AppManagerApp.TAG, "Completed AppRetriever... ");
        Log.i(AppManagerApp.TAG, "Total Apps Retreived......... " + integer);
        onPreparedListener.prepared();
    }

    public void bindInstalledAppFragment(InstalledAppFragment installedAppFragment) {
        this.onPreparedListener = installedAppFragment;
    }

    public interface OnPreparedListener {
        public void prepared();
    }

}
