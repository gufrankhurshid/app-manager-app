package com.khurshid.gufran.appmanager.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.khurshid.gufran.appmanager.R;

import java.util.List;

/**
 * Created by guffy on 1/15/16.
 */
public class HomeActivityPagerAdapter extends FragmentPagerAdapter {

    Context context;
    List<Fragment> fragmentList;

    public HomeActivityPagerAdapter(FragmentManager fm, Context context, List<Fragment> fragmentList) {
        super(fm);
        this.context = context;
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) {
            return context.getResources().getString(R.string.tab_title_installed_app);
        } else if (position == 1) {
            return context.getResources().getString(R.string.tab_title_uninstalled_app);
        } else {
            return context.getResources().getString(R.string.tab_title_updated_app);
        }
    }

}
