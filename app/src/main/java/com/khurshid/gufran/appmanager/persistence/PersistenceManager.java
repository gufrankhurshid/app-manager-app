package com.khurshid.gufran.appmanager.persistence;

import android.content.Context;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.khurshid.gufran.appmanager.persistence.dto.App;
import com.khurshid.gufran.appmanager.persistence.dto.InstalledApp;
import com.khurshid.gufran.appmanager.persistence.dto.RemovedApp;
import com.khurshid.gufran.appmanager.persistence.dto.Status;
import com.khurshid.gufran.appmanager.persistence.dto.UpdatingApp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guffy on 1/15/16.
 */
@Singleton
public class PersistenceManager {
    private Provider<Context> contextProvider;
    private DatabaseHelper dbHelper;
    private RuntimeExceptionDao<InstalledApp, Integer> installedAppRuntimeExceptionDao;
    private RuntimeExceptionDao<RemovedApp, Integer> removedAppRuntimeExceptionDao;
    private RuntimeExceptionDao<UpdatingApp, Integer> updatingAppRuntimeExceptionDao;


    public PersistenceManager(Context context) {
        dbHelper = OpenHelperManager
                .getHelper(context, DatabaseHelper.class);
        installedAppRuntimeExceptionDao = dbHelper
                .getInstalledAppRuntimeExceptionDao();
        removedAppRuntimeExceptionDao = dbHelper
                .getRemovedAppRuntimeExceptionDao();
        updatingAppRuntimeExceptionDao = dbHelper
                .getUpdatingAppRuntimeExceptionDao();
    }

    @Inject
    public PersistenceManager(Provider<Context> contextProvider) {
        this(contextProvider.get());
    }


    public boolean save(App app) {
        int r = -1;
        if (app instanceof InstalledApp) {
            InstalledApp installedApp = (InstalledApp) app;
            r = installedAppRuntimeExceptionDao.create(installedApp);
        } else if (app instanceof RemovedApp) {
            RemovedApp removedApp = (RemovedApp) app;
            r = removedAppRuntimeExceptionDao.create(removedApp);
        } else if (app instanceof UpdatingApp) {
            UpdatingApp updatingApp = (UpdatingApp) app;
            r = updatingAppRuntimeExceptionDao.create(updatingApp);
        }
        return ((r > 0) ? true : false);
    }

    public boolean update(App app) {
        int r = -1;
        if (app instanceof InstalledApp) {
            InstalledApp installedApp = (InstalledApp) app;
            r = installedAppRuntimeExceptionDao.update(installedApp);
        } else if (app instanceof RemovedApp) {
            RemovedApp removedApp = (RemovedApp) app;
            r = removedAppRuntimeExceptionDao.update(removedApp);
        } else if (app instanceof UpdatingApp) {
            UpdatingApp updatingApp = (UpdatingApp) app;
            r = updatingAppRuntimeExceptionDao.update(updatingApp);
        }
        return ((r > 0) ? true : false);
    }


    public List<App> getApp(String applicationId) {
        List<App> appList = new ArrayList<>();
        appList.addAll(installedAppRuntimeExceptionDao.queryForEq("applicationId", applicationId));
        appList.addAll(removedAppRuntimeExceptionDao.queryForEq("applicationId", applicationId));
        appList.addAll(updatingAppRuntimeExceptionDao.queryForEq("applicationId", applicationId));
        return appList;
    }

    public List<App> getApps(Status status) {
        List<App> appList = new ArrayList<>();
        appList.addAll(installedAppRuntimeExceptionDao.queryForEq("status", status));
        appList.addAll(removedAppRuntimeExceptionDao.queryForEq("status", status));
        appList.addAll(updatingAppRuntimeExceptionDao.queryForEq("status", status));
        return appList;
    }

    public List<App> getAllApps() {
        List<App> appList = new ArrayList<>();
        appList.addAll(installedAppRuntimeExceptionDao.queryForAll());
        appList.addAll(removedAppRuntimeExceptionDao.queryForAll());
        appList.addAll(updatingAppRuntimeExceptionDao.queryForAll());
        return appList;
    }


    public boolean delete(App app) {
        int r = -1;
        if (app instanceof InstalledApp) {
            InstalledApp installedApp = (InstalledApp) app;
            r = installedAppRuntimeExceptionDao.delete(installedApp);
        } else if (app instanceof RemovedApp) {
            RemovedApp removedApp = (RemovedApp) app;
            r = removedAppRuntimeExceptionDao.delete(removedApp);
        } else if (app instanceof UpdatingApp) {
            UpdatingApp updatingApp = (UpdatingApp) app;
            r = updatingAppRuntimeExceptionDao.delete(updatingApp);
        }
        return ((r > 0) ? true : false);
    }

}
