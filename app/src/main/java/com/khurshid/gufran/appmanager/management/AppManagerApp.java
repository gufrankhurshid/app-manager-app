package com.khurshid.gufran.appmanager.management;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;


/*
 *  Code Prepared by **Gufran Khurshid**.
 *  Sr Android Developer.
    Email Id : gufran.khurshid@gmail.com
    Skype Id : gufran.khurshid
    Code: **16 January, 2016.**

    Description: **Application class**

    All Rights Reserved.
*/
public class AppManagerApp extends Application {

    private static AppManagerApp appManagerApp;
    public static String TAG = "APP MANAGER";
    public static Context context;


    @Override
    public void onCreate() {
        super.onCreate();
        appManagerApp = AppManagerApp.this;
        Stetho.initializeWithDefaults(this);
    }

    public AppManagerApp getInstance() {
        return appManagerApp;
    }

    public static Context getAppContext() {
        return appManagerApp.getApplicationContext();
    }

}
