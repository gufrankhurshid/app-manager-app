package com.khurshid.gufran.appmanager.persistence.dto;

/**
 * Created by guffy on 1/15/16.
 */

import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "InstalledApp")
public class InstalledApp extends App {

    public InstalledApp() {
    }

    public InstalledApp(String name, String applicationId, String iconURI) {
        this.name = name;
        this.applicationId = applicationId;
        this.iconURI = iconURI;
        this.status = Status.INSTALLED;
    }


}
