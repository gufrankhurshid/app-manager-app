package com.khurshid.gufran.appmanager.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.khurshid.gufran.appmanager.management.AppManagerApp;

/**
 * Created by guffy on 1/16/16.
 */
//@Singleton
public class PackageUtil {
    private Provider<Context> contextProvider;
    Context context;

    @Inject
    public PackageUtil(Provider<Context> contextProvider) {
        context = contextProvider.get();
    }
    public PackageUtil(Context context) {
        this.context = context;
    }

    public ApplicationInfo getPackage(String uri) throws Exception {
        PackageManager packageManager = context.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return packageInfo.applicationInfo;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(AppManagerApp.TAG, "PackageManager.NameNotFoundException  " + e.getMessage());
            throw e;
        } catch (Exception e) {
            Log.e(AppManagerApp.TAG, "Exception " + e.getMessage());
            throw e;
        }
    }

    private boolean appPackageInstalled(String uri) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(AppManagerApp.TAG, "PackageManager.NameNotFoundException  " + e.getMessage());
            app_installed = false;
        }
        return app_installed;
    }

}
