package com.khurshid.gufran.appmanager.persistence.dto;

/**
 * Created by guffy on 1/15/16.
 */
public enum Status {
    INSTALLED, UPDATING, REMOVED;
}
