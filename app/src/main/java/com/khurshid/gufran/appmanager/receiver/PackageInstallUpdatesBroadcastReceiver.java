package com.khurshid.gufran.appmanager.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;

import com.khurshid.gufran.appmanager.dao.AppDaoImpl;
import com.khurshid.gufran.appmanager.helper.BitmapHelper;
import com.khurshid.gufran.appmanager.management.AppManagerApp;
import com.khurshid.gufran.appmanager.persistence.dto.InstalledApp;
import com.khurshid.gufran.appmanager.util.PackageUtil;

/**
 * Created by guffy on 1/16/16.
 */
public class PackageInstallUpdatesBroadcastReceiver extends BroadcastReceiver {

    private AppDaoImpl appDao;
    private PackageUtil packageUtil;
    private BitmapHelper bitmapHelper;
    private static final String TAG = AppManagerApp.TAG;
    private Context context;
    private int UPDATION_DELAY = 5000;
    static private UpdationState UPDATION_STATE = UpdationState.NOT_DEFIND;

    @Override

    public void onReceive(Context context, Intent intent) {
        this.context = context;
        appDao = new AppDaoImpl(context);
        bitmapHelper = new BitmapHelper(context);
        packageUtil = new PackageUtil(context);

        final String packageName = intent.getData().getSchemeSpecificPart();

        if (intent.getAction().equals(Intent.ACTION_PACKAGE_REPLACED)) {
            // Log.d(TAG, "Intent.ACTION_PACKAGE_REPLACED " + packageName);
            PackageInstallUpdatesBroadcastReceiver.UPDATION_STATE = UpdationState.REPLACED;
            execute(PackageInstallUpdatesBroadcastReceiver.UPDATION_STATE, packageName);

        } else if (intent.getAction().equals(Intent.ACTION_PACKAGE_ADDED)) {
            // Log.d(TAG, "Intent.ACTION_PACKAGE_ADDED " + packageName);
            PackageInstallUpdatesBroadcastReceiver.UPDATION_STATE = UpdationState.ADDED;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // wait for UPDATION_DELAY time to see and check whether or not it was REPLACE Action
                    if (PackageInstallUpdatesBroadcastReceiver.UPDATION_STATE != UpdationState.REPLACED) {
                        execute(UpdationState.ADDED, packageName);
                    }
                }
            }, UPDATION_DELAY);/// assuming the application updation doesnot take more than UPDATION_DELAY ms
        } else if (intent.getAction().equals(Intent.ACTION_PACKAGE_REMOVED)) {
            // Log.d(TAG, "Intent.ACTION_PACKAGE_REMOVED " + packageName);
            PackageInstallUpdatesBroadcastReceiver.UPDATION_STATE = UpdationState.REMOVED;
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    // wait for UPDATION_DELAY time to see and check whether or not it was REPLACE Action
                    if (PackageInstallUpdatesBroadcastReceiver.UPDATION_STATE != UpdationState.REPLACED) {
                        execute(UpdationState.REMOVED, packageName);
                    }
                }
            }, UPDATION_DELAY);/// assuming the application updation doesnot take more than UPDATION_DELAY ms
        }
    }

    private void execute(UpdationState updationState, String packageName) {
        try {
            if (updationState == UpdationState.ADDED) {
                final PackageManager pm = context.getPackageManager();
                ApplicationInfo applicationInfo = packageUtil.getPackage(packageName);
                String name;
                if ((name = String.valueOf(context.getPackageManager().getApplicationLabel(applicationInfo))).isEmpty()) {
                    name = applicationInfo.packageName;
                }
                packageName = applicationInfo.packageName;
                Drawable icon = pm.getApplicationIcon(applicationInfo);
                String iconPath = "";
                if (icon instanceof BitmapDrawable) {
                    BitmapDrawable bitmapDrawable = (BitmapDrawable) icon;
                    iconPath = bitmapHelper.storeIcon(bitmapDrawable.getBitmap());
                }
                InstalledApp installedApp = new InstalledApp(name, packageName, iconPath);
                boolean result = appDao.saveApp(installedApp);
                Log.d(TAG, packageName + " App Installed ,Result : " + result);
            } else if (updationState == UpdationState.REMOVED) {
                boolean result = appDao.convertToRemovedApp(packageName);
                Log.d(TAG, packageName + " App Removed ,Result : " + result);
            } else if (updationState == UpdationState.REPLACED) {
                boolean result = appDao.convertToUpdatingApp(packageName);
                Log.d(TAG, packageName + " App Updated ,Result : " + result);
            }
        } catch (Exception e) {
            Log.e(AppManagerApp.TAG, "Exception " + e.getMessage());
            e.printStackTrace();
        }
    }

}
