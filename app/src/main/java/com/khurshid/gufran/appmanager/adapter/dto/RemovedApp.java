package com.khurshid.gufran.appmanager.adapter.dto;

/**
 * Created by gufran on 17/1/16.
 */
public class RemovedApp {
    public String appName;
    public String fileUri;
    public String removalDateTimeString;

    public RemovedApp(String appName, String fileUri, String removalDateTimeString) {
        this.appName = appName;
        this.fileUri = fileUri;
        this.removalDateTimeString = removalDateTimeString;
    }
}
