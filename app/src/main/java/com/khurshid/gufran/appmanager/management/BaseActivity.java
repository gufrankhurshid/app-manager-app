package com.khurshid.gufran.appmanager.management;

import android.os.Bundle;

import roboguice.activity.RoboFragmentActivity;

/*
 *  Code Prepared by **Gufran Khurshid**.
 *  Sr Android Developer.
    Email Id : gufran.khurshid@gmail.com
    Skype Id : gufran.khurshid
    Code: **17 January, 2016.**

    Description: **BaseActivity is used to intialize common variables and call common methods**

    All Rights Reserved.
*/
public abstract class BaseActivity extends RoboFragmentActivity implements ActivitySetup {

    public String TAG = AppManagerApp.TAG;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        referView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initialize();
        applyTypeFace();
        setUpActionBar();
    }

}
