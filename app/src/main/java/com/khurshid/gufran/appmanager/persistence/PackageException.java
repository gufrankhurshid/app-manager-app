package com.khurshid.gufran.appmanager.persistence;

/**
 * Created by guffy on 1/16/16.
 */
public class PackageException extends Exception {
    public PackageException(String message) {
        super(message);
    }
}
