package com.khurshid.gufran.appmanager.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.Singleton;
import com.khurshid.gufran.appmanager.management.AppManagerApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by guffy on 1/17/16.
 */
@Singleton
public class BitmapHelper {

    private Provider<Context> contextProvider;
    Context context;

    @Inject
    public BitmapHelper(Provider<Context> contextProvider) {
        context = contextProvider.get();
    }

    public BitmapHelper(Context context) {
        this.context = context;
    }

    public String storeIcon(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(AppManagerApp.TAG,
                    "Error creating media file, check storage permissions: ");
        } else {
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.PNG, 90, fos);
                fos.flush();
                fos.close();
                return pictureFile.getPath();
            } catch (FileNotFoundException e) {
                Log.d(AppManagerApp.TAG, "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d(AppManagerApp.TAG, "Error accessing file: " + e.getMessage());
            }
        }
        return null;
    }


    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/OkuApp/Icons");
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdir();
        }
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmssSSS").format(new Date());
        File mediaFile;
        String mImageName = "ICON_" + timeStamp + ".png";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


}
